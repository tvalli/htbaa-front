import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FooterComponent } from './ui/containers/footer/footer.component';
import { HeaderComponent } from './ui/containers/header/header.component';
import { NavComponent } from './ui/containers/nav/nav.component';
import { PageUiComponent } from './ui/pages/page-ui/page-ui.component';


@NgModule({
  declarations: [
    AppComponent,
    PageUiComponent,
    FooterComponent,
    HeaderComponent,
    NavComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
