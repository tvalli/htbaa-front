import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { User } from '../../models/User';
import { UserServiceService } from '../../services/user-service.service';

@Component({
  selector: 'app-page-add-user',
  templateUrl: './page-add-user.component.html',
  styleUrls: ['./page-add-user.component.scss']
})
export class PageAddUserComponent implements OnInit {

  userForm = new FormGroup({
    name: new FormControl(),
    email: new FormControl()
  });

  constructor(private userService: UserServiceService) { }

  ngOnInit(): void {
  }

  onSubmit(): void {
    console.log('adding user...');

    const user = new User({ name: this.userForm.value.name, email: this.userForm.value.email });

    this.userService.addUser(user).subscribe();
  }

}
