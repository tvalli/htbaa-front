import { Component, OnInit } from '@angular/core';
import { User } from '../../models/User';
import { UserServiceService } from '../../services/user-service.service';

@Component({
  selector: 'app-page-user',
  templateUrl: './page-user.component.html',
  styleUrls: ['./page-user.component.scss']
})
export class PageUserComponent implements OnInit {

  userList: User[];

  constructor(private userService: UserServiceService) { }

  ngOnInit(): void {
  }

  refreshUserList(): void {

    this.userService.findAllUser().subscribe((userList: User[]) => this.userList = userList);

  }

}
