import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { PageAddUserComponent } from './pages/page-add-user/page-add-user.component';
import { PageUserComponent } from './pages/page-user/page-user.component';
import { UserRoutingModule } from './user-routing.module';


@NgModule({
  declarations: [PageUserComponent, PageAddUserComponent],
  imports: [
    CommonModule,
    UserRoutingModule,
    ReactiveFormsModule,
  ]
})
export class UserModule { }
