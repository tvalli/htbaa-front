import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../models/User';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {

  constructor(private http: HttpClient) { }


  addUser(user: User): Observable<void> {

    return this.http.post<void>('/users', user);

  }

  findAllUser(): Observable<User[]> {

    return this.http.get<User[]>('/users');

  }
}
