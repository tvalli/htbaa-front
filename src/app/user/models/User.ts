export class User {

  'id': number;
  'name': string;
  'email': string;

  constructor(fields?: Partial<User>) {
    if (fields) {
      Object.assign(this, fields);
    }
  }

}
